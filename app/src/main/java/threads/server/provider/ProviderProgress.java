package threads.server.provider;

import android.os.CancellationSignal;

import androidx.annotation.Nullable;

import threads.server.ipfs.ReaderProgress;

public class ProviderProgress implements ReaderProgress {

    private final CancellationSignal signal;
    long totalRead = 0L;

    public ProviderProgress(@Nullable CancellationSignal signal) {
        this.signal = signal;
    }

    @Override
    public long getSize() {
        return 0;
    }

    @Override
    public void setProgress(int progress) {
    }

    @Override
    public boolean doProgress() {
        return false;
    }

    @Override
    public boolean isClosed() {
        if (signal != null) {
            return signal.isCanceled();
        }
        return false;
    }

    @Override
    public boolean isOffline() {
        return false;
    }

    public long getTotalRead() {
        return this.totalRead;
    }

    @Override
    public void setTotalRead(long totalRead) {
        this.totalRead = totalRead;
    }
}
