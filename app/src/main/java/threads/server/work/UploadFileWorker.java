package threads.server.work;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.OutputStream;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import threads.LogUtils;
import threads.server.InitApplication;
import threads.server.R;
import threads.server.core.peers.Content;
import threads.server.core.threads.THREADS;
import threads.server.core.threads.Thread;
import threads.server.ipfs.CID;
import threads.server.ipfs.IPFS;
import threads.server.ipfs.ReaderProgress;

public class UploadFileWorker extends Worker {
    private static final String WID = "UFW";
    private static final String TAG = UploadFileWorker.class.getSimpleName();
    private final NotificationManager mNotificationManager;
    private final AtomicReference<Notification> mLastNotification = new AtomicReference<>(null);

    @SuppressWarnings("WeakerAccess")
    public UploadFileWorker(@NonNull Context context,
                            @NonNull WorkerParameters params) {
        super(context, params);
        mNotificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
    }


    private static OneTimeWorkRequest getWork(@NonNull Uri uri, long idx) {

        Data.Builder data = new Data.Builder();
        data.putLong(Content.IDX, idx);
        data.putString(Content.URI, uri.toString());

        return new OneTimeWorkRequest.Builder(UploadFileWorker.class)
                .addTag(TAG)
                .setInputData(data.build())
                .setInitialDelay(1, TimeUnit.MILLISECONDS)
                .build();
    }

    public static void load(@NonNull Context context, @NonNull Uri uri, long idx) {
        WorkManager.getInstance(context).enqueueUniqueWork(
                WID + idx, ExistingWorkPolicy.KEEP, getWork(uri, idx));

    }

    private void closeNotification(long idx) {
        if (mNotificationManager != null) {
            mNotificationManager.cancel((int) idx);
        }
    }

    private void reportProgress(long idx, @NonNull String title, int percent) {

        if (!isStopped()) {
            Notification notification = createNotification(title, percent);
            mLastNotification.set(notification);

            if (mNotificationManager != null) {
                mNotificationManager.notify((int) idx, notification);
            }
        }
    }

    private Notification createNotification(@NonNull String text, int progress) {

        Notification.Builder builder;
        if (mLastNotification.get() != null) {
            builder = Notification.Builder.recoverBuilder(
                    getApplicationContext(), mLastNotification.get());
            builder.setProgress(100, progress, false);
            builder.setContentText(text);
            builder.setSubText("" + progress + "%");
            return builder.build();
        } else {
            builder = new Notification.Builder(getApplicationContext(), InitApplication.CHANNEL_ID);
        }

        builder.setContentText(text)
                .setSubText("" + progress + "%")
                .setProgress(100, progress, false)
                .setOnlyAlertOnce(true)
                .setSmallIcon(R.drawable.download)
                .setCategory(Notification.CATEGORY_PROGRESS)
                .setUsesChronometer(true)
                .setOngoing(true);

        return builder.build();
    }

    @NonNull
    @Override
    public Result doWork() {

        long idx = getInputData().getLong(Content.IDX, -1);
        String uri = getInputData().getString(Content.URI);
        Objects.requireNonNull(uri);

        long start = System.currentTimeMillis();
        LogUtils.info(TAG, " start ... " + idx);

        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            THREADS threads = THREADS.getInstance(getApplicationContext());


            Thread thread = threads.getThreadByIdx(idx);
            Objects.requireNonNull(thread);

            CID cid = thread.getContent();
            Objects.requireNonNull(cid);

            long size = thread.getSize();

            boolean showProgress = size > 100 * 1000 * 1000; // 100 MB


            try (OutputStream os = getApplicationContext().getContentResolver().
                    openOutputStream(Uri.parse(uri))) {
                Objects.requireNonNull(os);
                ipfs.storeToOutputStream(os, new ReaderProgress() {
                    @Override
                    public long getSize() {
                        return thread.getSize();
                    }

                    @Override
                    public void setTotalRead(long totalRead) {

                    }

                    @Override
                    public void setProgress(int percent) {
                        reportProgress(idx, thread.getName(), percent);
                    }

                    @Override
                    public boolean doProgress() {
                        return !isStopped() && showProgress;
                    }

                    @Override
                    public boolean isClosed() {
                        return false;
                    }

                    @Override
                    public boolean isOffline() {
                        return false;
                    }
                }, cid, 4096);
            } catch (Throwable e) {
                LogUtils.error(TAG, e);
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
            return Result.failure();
        } finally {
            closeNotification(idx);
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();

    }
}
