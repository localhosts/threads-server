package threads.server.work;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import threads.LogUtils;
import threads.server.InitApplication;
import threads.server.core.peers.Content;
import threads.server.core.peers.PEERS;
import threads.server.core.peers.User;
import threads.server.ipfs.IPFS;
import threads.server.ipfs.PID;
import threads.server.ipfs.PeerInfo;

public class SwarmConnectWorker extends Worker {

    private static final String WID = "SCW";
    private static final String TAG = SwarmConnectWorker.class.getSimpleName();


    @SuppressWarnings("WeakerAccess")
    public SwarmConnectWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }

    private static String getUniqueId(@NonNull String pid) {
        return WID + pid;
    }

    private static OneTimeWorkRequest getWork(@NonNull String pid,
                                              @NonNull String host, int port, boolean inet6) {

        Data.Builder data = new Data.Builder();
        data.putString(Content.PID, pid);
        data.putString(Content.HOST, host);
        data.putInt(Content.PORT, port);
        data.putBoolean(Content.INET6, inet6);

        return new OneTimeWorkRequest.Builder(SwarmConnectWorker.class)
                .setInputData(data.build())
                .setInitialDelay(1, TimeUnit.SECONDS)
                .addTag(TAG)
                .build();
    }

    public static void connect(@NonNull Context context, @NonNull String pid,
                               @NonNull String host, int port, boolean inet6) {

        WorkManager.getInstance(context).enqueueUniqueWork(
                getUniqueId(pid), ExistingWorkPolicy.KEEP, getWork(pid, host, port, inet6));
    }


    @NonNull
    @Override
    public Result doWork() {

        String pid = getInputData().getString(Content.PID);
        Objects.requireNonNull(pid);
        String host = getInputData().getString(Content.HOST);
        int port = getInputData().getInt(Content.PORT, 0);
        boolean inet6 = getInputData().getBoolean(Content.INET6, false);
        long start = System.currentTimeMillis();

        LogUtils.info(TAG, " start connect [" + pid + "]...");

        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            if (!ipfs.isConnected(PID.create(pid))) {

                String pre = "/ip4";
                if (inet6) {
                    pre = "/ip6";
                }
                String multiAddress = pre + host + "/tcp/" + port + "/p2p/" + pid;

                int timeout = InitApplication.getConnectionTimeout(getApplicationContext());
                ipfs.swarmConnect(multiAddress, timeout);
            }

            if (ipfs.isConnected(PID.create(pid))) {
                if (InitApplication.isAutoDiscovery(getApplicationContext())) {
                    PEERS peers = PEERS.getInstance(getApplicationContext());

                    User user = peers.getUserByPID(PID.create(pid));
                    if (user == null) {
                        user = peers.createUser(PID.create(pid), pid);
                        peers.storeUser(user);


                        PeerInfo pInfo = ipfs.pidInfo(PID.create(pid));
                        if (pInfo != null) {

                            if (peers.getUserPublicKey(pid) == null) {
                                String pKey = pInfo.getPublicKey();
                                if (pKey != null) {
                                    if (!pKey.isEmpty()) {
                                        peers.setUserPublicKey(pid, pKey);
                                    }
                                }
                            }
                            if (!peers.getUserIsLite(pid)) {

                                String agent = pInfo.getAgentVersion();
                                if (!agent.isEmpty()) {
                                    peers.setUserAgent(pid, agent);
                                    if (agent.endsWith("lite")) {
                                        peers.setUserLite(pid);
                                    }
                                }
                            }
                        }

                    }
                }
            }


        } catch (Throwable e) {
            LogUtils.error(TAG, e);
            return Result.failure();
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();
    }
}

