package threads.server.ipfs;

public interface ReaderProgress extends Progress {
    long getSize();

    void setTotalRead(long totalRead);
}
