package threads.server.ipfs;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.Gson;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import lite.DhtClose;
import lite.Listener;
import lite.Loader;
import lite.LsInfoClose;
import lite.Node;
import lite.Reader;
import lite.ResolveInfo;
import threads.LogUtils;

public class IPFS implements Listener {
    @NonNull
    public static final List<String> DNS_ADDRS = new ArrayList<>();

    @NonNull
    public static final List<String> Bootstrap = new ArrayList<>(Arrays.asList(
            "/ip4/147.75.80.110/tcp/4001/p2p/QmbFgm5zan8P6eWWmeyfncR5feYEMPbht5b1FW1C37aQ7y", // default relay  libp2p
            "/ip4/147.75.195.153/tcp/4001/p2p/QmW9m57aiBDHAkKj9nmFSEn7ZqrcF1fZS4bipsTCHburei",// default relay  libp2p
            "/ip4/147.75.70.221/tcp/4001/p2p/Qme8g49gm3q4Acp7xWBKg3nAa9fxZ1YmyDJdyGgoG6LsXh",// default relay  libp2p

            "/ip4/104.131.131.82/tcp/4001/p2p/QmaCpDMGvV2BGHeYERUEnRQAwe3N8SzbUtfsmvsqQLuvuJ"// mars.i.ipfs.io

    ));
    private static final String EMPTY_DIR_58 = "QmUNLLsPACCz1vLxQVkXqqLX5R1X345qqfHbsf67hvA3Nn";
    private static final String EMPTY_DIR_32 = "bafybeiczsscdsbs7ffqz55asqdf3smv6klcw3gofszvwlyarci47bgf354";
    private static final String PREF_KEY = "prefKey";
    private static final String PID_KEY = "pidKey";
    private static final String PRIVATE_NETWORK_KEY = "privateNetworkKey";
    private static final String HIGH_WATER_KEY = "highWaterKey";
    private static final String LOW_WATER_KEY = "lowWaterKey";
    private static final String GRACE_PERIOD_KEY = "gracePeriodKey";
    private static final String STORAGE_DIRECTORY = "storageDirectoryKey";
    private static final String SWARM_KEY = "swarmKey";
    private static final String SWARM_PORT_KEY = "swarmPortKey";
    private static final String PUBLIC_KEY = "publicKey";
    private static final String AGENT_KEY = "agentKey";
    private static final String PRIVATE_KEY = "privateKey";
    private static final String P2P_CIRCUIT = "p2p-circuit";
    private static final long TIMEOUT = 30000L;
    private static final String TAG = IPFS.class.getSimpleName();
    private static IPFS INSTANCE = null;
    private final File baseDir;
    private final File cacheDir;
    private final Node node;
    private final Object locker = new Object();
    private final Gson gson = new Gson();
    private final int location;
    private long seeded = 0L;

    @NonNull
    private Reachable mReachable = Reachable.UNKNOWN;

    private IPFS(@NonNull Context context) throws Exception {

        File dir = getExternalStorageDirectory(context);

        if (dir == null) {
            this.baseDir = context.getFilesDir();
            this.location = 0;
        } else {
            StorageManager manager = (StorageManager)
                    context.getSystemService(Activity.STORAGE_SERVICE);
            Objects.requireNonNull(manager);
            StorageVolume volume = manager.getStorageVolume(dir);
            Objects.requireNonNull(volume);
            this.location = volume.hashCode();
            this.baseDir = dir;
        }

        this.cacheDir = context.getCacheDir();


        PID host = getPID(context);

        boolean init = host == null;

        node = new Node(this, this.baseDir.getAbsolutePath());

        if (init) {
            node.identity();

            setPeerID(context, node.getPeerID());
            setPublicKey(context, node.getPublicKey());
            setPrivateKey(context, node.getPrivateKey());
        } else {
            node.setPeerID(host.getPid());
            node.setPrivateKey(IPFS.getPrivateKey(context));
            node.setPublicKey(IPFS.getPublicKey(context));
        }

        /* addNoAnnounce
         "/ip4/10.0.0.0/ipcidr/8",
                "/ip4/100.64.0.0/ipcidr/10",
                "/ip4/169.254.0.0/ipcidr/16",
                "/ip4/172.16.0.0/ipcidr/12",
                "/ip4/192.0.0.0/ipcidr/24",
                "/ip4/192.0.0.0/ipcidr/29",
                "/ip4/192.0.0.8/ipcidr/32",
                "/ip4/192.0.0.170/ipcidr/32",
                "/ip4/192.0.0.171/ipcidr/32",
                "/ip4/192.0.2.0/ipcidr/24",
                "/ip4/192.168.0.0/ipcidr/16",
                "/ip4/198.18.0.0/ipcidr/15",
                "/ip4/198.51.100.0/ipcidr/24",
                "/ip4/203.0.113.0/ipcidr/24",
                "/ip4/240.0.0.0/ipcidr/4"
         */


        String swarmKey = getSwarmKey(context);
        if (!swarmKey.isEmpty()) {
            node.setSwarmKey(swarmKey.getBytes());
            node.setEnablePrivateNetwork(isPrivateNetworkEnabled(context));
        }

        node.setAgent(IPFS.getStoredAgent(context));

        node.setPort(IPFS.getSwarmPort(context));

        node.setGracePeriod(getGracePeriod(context));
        node.setHighWater(getHighWater(context));
        node.setLowWater(getLowWater(context));


        node.openDatabase();
    }

    public static int getSwarmPort(@NonNull Context context) {

        SharedPreferences sharedPref = context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);
        return sharedPref.getInt(SWARM_PORT_KEY, 5001);
    }

    @SuppressWarnings("UnusedReturnValue")
    public static long copy(InputStream source, OutputStream sink) throws IOException {
        long nread = 0L;
        byte[] buf = new byte[4096];
        int n;
        while ((n = source.read(buf)) > 0) {
            sink.write(buf, 0, n);
            nread += n;
        }
        return nread;
    }

    @Nullable
    public static File getExternalStorageDirectory(@NonNull Context context) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        String dir = sharedPref.getString(STORAGE_DIRECTORY, "");
        Objects.requireNonNull(dir);
        if (dir.isEmpty()) {
            return null;
        }
        return new File(dir);
    }

    public static void setExternalStorageDirectory(@NonNull Context context, @Nullable File file) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        if (file == null) {
            editor.putString(STORAGE_DIRECTORY, "");
        } else {
            editor.putString(STORAGE_DIRECTORY, file.getAbsolutePath());
        }
        editor.apply();

    }

    private static void setPublicKey(@NonNull Context context, @NonNull String key) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(PUBLIC_KEY, key);
        editor.apply();
    }

    private static String getStoredAgent(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        return sharedPref.getString(AGENT_KEY, "go-ipfs/0.8.0-dev/lite");

    }

    private static void setPrivateKey(@NonNull Context context, @NonNull String key) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(PRIVATE_KEY, key);
        editor.apply();
    }

    @NonNull
    public static String getSwarmKey(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        return Objects.requireNonNull(sharedPref.getString(SWARM_KEY, ""));

    }

    public static void setSwarmKey(@NonNull Context context, @NonNull String key) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(SWARM_KEY, key);
        editor.apply();
    }

    @NonNull
    private static String getPublicKey(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        return Objects.requireNonNull(sharedPref.getString(PUBLIC_KEY, ""));

    }

    @NonNull
    private static String getPrivateKey(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        return Objects.requireNonNull(sharedPref.getString(PRIVATE_KEY, ""));

    }

    private static void setPeerID(@NonNull Context context, @NonNull String peerID) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(PID_KEY, peerID);
        editor.apply();
    }

    @Nullable
    public static PID getPID(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        String pid = sharedPref.getString(PID_KEY, "");
        Objects.requireNonNull(pid);
        if (pid.isEmpty()) {
            return null;
        }
        return PID.create(pid);
    }

    public static void setLowWater(@NonNull Context context, int lowWater) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(LOW_WATER_KEY, lowWater);
        editor.apply();
    }

    private static int getLowWater(@NonNull Context context) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        return sharedPref.getInt(LOW_WATER_KEY, 20);
    }

    public static void setPrivateNetworkEnabled(@NonNull Context context, boolean privateNetwork) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(PRIVATE_NETWORK_KEY, privateNetwork);
        editor.apply();
    }

    public static boolean isPrivateNetworkEnabled(@NonNull Context context) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        return sharedPref.getBoolean(PRIVATE_NETWORK_KEY, false);
    }

    @NonNull
    private static String getGracePeriod(@NonNull Context context) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        return Objects.requireNonNull(sharedPref.getString(GRACE_PERIOD_KEY, "30s"));
    }

    public static void setGracePeriod(@NonNull Context context, @NonNull String gracePeriod) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(GRACE_PERIOD_KEY, gracePeriod);
        editor.apply();

    }

    public static void setHighWater(@NonNull Context context, int highWater) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(HIGH_WATER_KEY, highWater);
        editor.apply();
    }

    private static int getHighWater(@NonNull Context context) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        return sharedPref.getInt(HIGH_WATER_KEY, 40);
    }

    @NonNull
    public static IPFS getInstance(@NonNull Context context) {
        if (INSTANCE == null) {
            synchronized (IPFS.class) {
                if (INSTANCE == null) {
                    try {
                        INSTANCE = new IPFS(context);
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }
        return INSTANCE;
    }

    private static int nextFreePort() {
        int port = ThreadLocalRandom.current().nextInt(4001, 65535);
        while (true) {
            if (isLocalPortFree(port)) {
                return port;
            } else {
                port = ThreadLocalRandom.current().nextInt(4001, 65535);
            }
        }
    }

    private static boolean isLocalPortFree(int port) {
        try {
            new ServerSocket(port).close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public static void logCacheDir(@NonNull Context context) {
        try {
            File[] files = context.getCacheDir().listFiles();
            if (files != null) {
                for (File file : files) {
                    LogUtils.error(TAG, "" + file.length() + " " + file.getAbsolutePath());
                    if (file.isDirectory()) {
                        File[] children = file.listFiles();
                        if (children != null) {
                            for (File child : children) {
                                LogUtils.error(TAG, "" + child.length() + " " + child.getAbsolutePath());
                            }
                        }
                    }
                }
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
    }

    public static void logBaseDir(@NonNull Context context) {
        try {
            File[] files = context.getFilesDir().listFiles();
            if (files != null) {
                for (File file : files) {
                    LogUtils.warning(TAG, "" + file.length() + " " + file.getAbsolutePath());
                    if (file.isDirectory()) {
                        logDir(file);
                    }
                }
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
    }

    public static void logDir(File file) {
        File[] children = file.listFiles();
        if (children != null) {
            for (File child : children) {
                LogUtils.warning(TAG, "" + child.length() + " " + child.getAbsolutePath());
                if (child.isDirectory()) {
                    logDir(child);
                }
            }
        }
    }

    private static void deleteFile(@NonNull File root) {
        try {
            if (root.isDirectory()) {
                File[] files = root.listFiles();
                if (files != null) {
                    for (File file : files) {
                        if (file.isDirectory()) {
                            deleteFile(file);
                            boolean result = file.delete();
                            if (!result) {
                                LogUtils.error(TAG, "File " + file.getName() + " not deleted");
                            }
                        } else {
                            boolean result = file.delete();
                            if (!result) {
                                LogUtils.error(TAG, "File " + file.getName() + " not deleted");
                            }
                        }
                    }
                }
                boolean result = root.delete();
                if (!result) {
                    LogUtils.error(TAG, "File " + root.getName() + " not deleted");
                }
            } else {
                boolean result = root.delete();
                if (!result) {
                    LogUtils.error(TAG, "File " + root.getName() + " not deleted");
                }
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
    }

    static void cleanCacheDir(@NonNull Context context) {

        try {
            File[] files = context.getCacheDir().listFiles();
            if (files != null) {
                for (File file : files) {
                    if (file.isDirectory()) {
                        deleteFile(file);
                        boolean result = file.delete();
                        if (!result) {
                            LogUtils.error(TAG, "File not deleted.");
                        }
                    } else {
                        boolean result = file.delete();
                        if (!result) {
                            LogUtils.error(TAG, "File not deleted.");
                        }
                    }
                }
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
    }

    public String getHost() {
        return base32(node.getPeerID());
    }

    public void checkSwarmKey(@NonNull String key) throws Exception {
        node.checkSwarmKey(key);
    }


    public synchronized void startDaemon() {
        if (!node.getRunning()) {
            synchronized (locker) {
                if (!node.getRunning()) {
                    AtomicBoolean failure = new AtomicBoolean(false);
                    ExecutorService executor = Executors.newSingleThreadExecutor();
                    AtomicReference<String> exception = new AtomicReference<>("");
                    executor.submit(() -> {
                        try {
                            long port = node.getPort();
                            if (!isLocalPortFree((int) port)) {
                                node.setPort(nextFreePort());
                            }

                            node.daemon();
                        } catch (Throwable e) {
                            failure.set(true);
                            exception.set("" + e.getLocalizedMessage());
                            LogUtils.error(TAG, e);
                        }
                    });
                    long time = 0L;
                    while (!node.getRunning() && time <= TIMEOUT && !failure.get()) {
                        time = time + 100L;
                        try {
                            Thread.sleep(100);
                        } catch (Throwable e) {
                            throw new RuntimeException(exception.get());
                        }
                    }
                    if (failure.get()) {
                        throw new RuntimeException(exception.get());
                    }
                }
            }
        }
    }

    @NonNull
    public Reachable getReachable() {
        return mReachable;
    }

    private void setReachable(@NonNull Reachable reachable) {
        mReachable = reachable;
    }

    public int getLocation() {
        return location;
    }

    public long getTotalSpace() {
        return this.baseDir.getTotalSpace();
    }

    public long getFreeSpace() {
        return this.baseDir.getFreeSpace();
    }

    void bootstrap(int timeout) {

        checkDaemon();

        for (String address : Bootstrap) {
            boolean result = swarmConnect(address, timeout);
            LogUtils.debug(TAG, result + " \n Bootstrap : " + address);
        }

        List<String> multiAddresses = DnsAddrResolver.getMultiAddresses();
        for (String address : multiAddresses) {

            boolean result = swarmConnect(address, timeout);
            LogUtils.debug(TAG, result + " \n Bootstrap : " + address);
        }
    }

    private void checkDaemon() {
        if (!isDaemonRunning()) {
            startDaemon();
        }
    }

    public List<PID> dhtFindProviders(@NonNull CID cid, int numProvs, int timeout) {

        checkDaemon();

        List<PID> providers = new ArrayList<>();

        try {

            node.dhtFindProvsTimeout(cid.getCid(), (pid) ->
                            providers.add(PID.create(pid))
                    , numProvs, timeout);
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
        return providers;
    }

    public void dhtPublish(@NonNull CID cid, @NonNull DhtClose closable) {

        checkDaemon();

        try {
            node.dhtProvide(cid.getCid(), closable);
        } catch (Throwable ignore) {
        }
    }

    @Nullable
    public PeerInfo id(@NonNull Peer peer, int timeout) {

        checkDaemon();
        return id(peer.getPid(), timeout);
    }

    @Nullable
    public PeerInfo pidInfo(@NonNull PID pid) {

        checkDaemon();
        try {
            String json = node.pidInfo(pid.getPid());
            //noinspection rawtypes
            Map map = gson.fromJson(json, Map.class);
            return PeerInfo.create(map);

        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }

        return null;

    }

    @Nullable
    public PeerInfo id(@NonNull PID pid, int timeout) {

        checkDaemon();
        try {
            String json = node.idWithTimeout(pid.getPid(), timeout);
            Map map = gson.fromJson(json, Map.class);
            return PeerInfo.create(map);

        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }

        return null;

    }

    @Nullable
    public PeerInfo id() {

        checkDaemon();
        try {
            String json = node.id();
            Map map = gson.fromJson(json, Map.class);
            return PeerInfo.create(map);
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    boolean checkRelay(@NonNull Peer peer, int timeout) {

        checkDaemon();

        AtomicBoolean success = new AtomicBoolean(false);
        //noinspection CatchMayIgnoreException
        try {

            PID pid = PID.create("QmWATWQ7fVPP2EFGu71UkfnqhYXDYH566qy47CnJDgvs8u"); // DUMMY

            String address = relayAddress(peer, pid);
            node.swarmConnect(address, timeout);

        } catch (Throwable e) {
            String line = e.getLocalizedMessage();
            if (line != null) {
                if (line.contains("HOP_NO_CONN_TO_DST")) {
                    success.set(true);
                }
            }
        }
        return success.get();
    }

    @NonNull
    private String relayAddress(@NonNull Peer relay, @NonNull PID pid) {

        return relayAddress(relay.getMultiAddress(), relay.getPid(), pid);
    }

    @NonNull
    private String relayAddress(@NonNull String address, @NonNull PID relay, @NonNull PID pid) {

        return address + "/p2p/" + relay.getPid() +
                "/" + P2P_CIRCUIT + "/p2p/" + pid.getPid();
    }

    private boolean specificRelay(@NonNull PID relay,
                                  @NonNull PID pid,
                                  int timeout) {

        checkDaemon();

        if (swarmConnect(relay, timeout)) {
            Peer peer = swarmPeer(relay);
            if (peer != null) {
                return swarmConnect(relayAddress(peer, pid), timeout);
            }
        }


        return false;
    }

    public boolean relay(@NonNull PID relay, @NonNull PID pid, int timeout) {
        checkDaemon();

        return specificRelay(relay, pid, timeout);

    }

    public boolean swarmConnect(@NonNull PID pid, int timeout) {
        checkDaemon();
        return swarmConnect("/p2p/" +
                pid.getPid(), timeout);
    }

    public boolean swarmConnect(@NonNull Peer peer, int timeout) {
        checkDaemon();
        String ma = peer.getMultiAddress() + "/p2p/" + peer.getPid();
        return swarmConnect(ma, timeout);

    }

    public boolean swarmConnect(@NonNull String multiAddress, int timeout) {
        checkDaemon();
        try {
            return node.swarmConnect(multiAddress, timeout);
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
        return false;
    }


    public boolean isPrivateNetwork() {
        return node.getPrivateNetwork();
    }

    public boolean isConnected(@NonNull PID pid) {

        if (!isDaemonRunning()) {
            return false;
        }
        try {
            return node.isConnected(pid.getPid());
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
        return false;
    }

    @Nullable
    public Peer swarmPeer(@NonNull PID pid) {
        checkDaemon();
        try {
            String json = node.swarmPeer(pid.getPid());
            if (json != null && !json.isEmpty()) {
                Map map = gson.fromJson(json, Map.class);
                if (map != null) {
                    return Peer.create(map);
                }
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
        return null;
    }

    @NonNull
    public List<Peer> swarmPeers() {
        checkDaemon();
        List<Peer> peers = swarm_peers();
        peers.sort(Peer::compareTo);
        return peers;
    }

    @NonNull
    private List<Peer> swarm_peers() {

        List<Peer> peers = new ArrayList<>();
        if (isDaemonRunning()) {
            try {
                String json = node.swarmPeers();
                if (json != null && !json.isEmpty()) {
                    Map map = gson.fromJson(json, Map.class);
                    if (map != null) {

                        Object object = map.get("Peers");
                        if (object instanceof List) {
                            List list = (List) object;
                            if (!list.isEmpty()) {
                                for (Object entry : list) {
                                    if (entry instanceof Map) {
                                        Map peer = (Map) entry;
                                        peers.add(Peer.create(peer));
                                    }
                                }
                            }
                        }
                    }
                }

            } catch (Throwable e) {
                LogUtils.error(TAG, e);
            }
        }
        return peers;
    }

    public void publishName(@NonNull CID cid, @NonNull Closeable closeable) {
        checkDaemon();
        try {
            node.publishName(cid.getCid(), closeable::isClosed);
        } catch (Throwable ignore) {
        }
    }

    @NonNull
    public String base32(@NonNull String pid) {
        try {
            return node.pidToCid(pid);
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    @Nullable
    public CID resolveName(@NonNull String name, @NonNull Closeable closeable) {
        checkDaemon();
        long time = System.currentTimeMillis();


        LogUtils.error(TAG, "Resolve name " + name);

        AtomicReference<String> resolvedName = new AtomicReference<>(null);
        try {

            AtomicBoolean close = new AtomicBoolean(false);
            node.resolveName(new ResolveInfo() {
                @Override
                public boolean close() {
                    return close.get() || closeable.isClosed();
                }

                @Override
                public void resolved(String hash) {
                    if (hash.startsWith("/ipfs/")) {
                        hash = hash.replaceFirst("/ipfs/", "");
                        LogUtils.error(TAG, "resolved hash " + hash
                                + " " + (System.currentTimeMillis() - time));
                        if (Objects.equals(resolvedName.get(), hash) || closeable.isOffline()) {
                            close.set(true);
                        }
                        resolvedName.set(hash);
                    }

                }
            }, name, closeable.isOffline(), 8);

        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
        LogUtils.error(TAG, "Finished resolve name " + name + " " + (System.currentTimeMillis() - time));
        String hash = resolvedName.get();
        if (hash != null) {
            return CID.create(hash);
        }
        return null;
    }


    public void rm(@NonNull String cid, boolean recursively) {

        try {
            node.rm(cid, recursively);
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
    }

    public long getSwarmPort() {
        return node.getPort();
    }

    @Nullable
    public CID storeData(@NonNull byte[] data) {

        try (InputStream inputStream = new ByteArrayInputStream(data)) {
            return storeInputStream(inputStream);
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
        return null;
    }

    @Nullable
    public CID storeText(@NonNull String content) {

        try (InputStream inputStream = new ByteArrayInputStream(content.getBytes())) {
            return storeInputStream(inputStream);
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
        return null;
    }

    @Nullable
    public CID rmLinkFromDir(CID dir, String name) {
        try {
            return CID.create(node.removeLinkFromDir(dir.getCid(), name));
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
        return null;
    }

    @Nullable
    public CID addLinkToDir(@NonNull CID dir, @NonNull String name, @NonNull CID link) {
        try {
            return CID.create(node.addLinkToDir(dir.getCid(), name, link.getCid()));
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
        return null;
    }

    @Nullable
    public CID createEmptyDir() {
        try {
            return CID.create(node.createEmptyDir());
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
        return null;
    }

    @Nullable
    public LinkInfo getLinkInfo(@NonNull CID dir, @NonNull List<String> path, @NonNull Closeable closeable) {

        LinkInfo linkInfo = null;
        CID root = dir;

        for (String name : path) {
            linkInfo = getLinkInfo(root, name, closeable);
            if (linkInfo != null) {
                root = linkInfo.getCid();
            } else {
                break;
            }
        }

        return linkInfo;
    }

    @Nullable
    public LinkInfo getLinkInfo(@NonNull CID dir, @NonNull String name, @NonNull Closeable closeable) {
        List<LinkInfo> links = ls(dir, closeable);
        if (links != null) {
            for (LinkInfo info : links) {
                if (Objects.equals(info.getName(), name)) {
                    return info;
                }
            }
        }
        return null;
    }

    @Nullable
    public List<LinkInfo> getLinks(@NonNull CID cid, @NonNull Closeable closeable) {

        LogUtils.info(TAG, "Lookup CID : " + cid.getCid());

        List<LinkInfo> links = ls(cid, closeable);
        if (links == null) {
            LogUtils.info(TAG, "no links or stopped");
            return null;
        }

        List<LinkInfo> result = new ArrayList<>();
        for (LinkInfo link : links) {
            LogUtils.info(TAG, "Link : " + link.toString());
            if (!link.getName().isEmpty()) {
                result.add(link);
            }
        }
        return result;
    }

    @Nullable
    public List<LinkInfo> ls(@NonNull CID cid, @NonNull Closeable closeable) {
        checkDaemon();
        List<LinkInfo> infoList = new ArrayList<>();
        try {

            node.ls(cid.getCid(), new LsInfoClose() {
                @Override
                public boolean close() {
                    return closeable.isClosed();
                }

                @Override
                public void lsInfo(String name, String hash, long size, int type) {
                    LinkInfo info = LinkInfo.create(name, hash, size, type);
                    infoList.add(info);
                }
            });

        } catch (Throwable e) {
            LogUtils.error(TAG, e);
            return null;
        }
        if (closeable.isClosed()) {
            return null;
        }
        return infoList;
    }

    @Nullable
    public CID storeFile(@NonNull File target) {

        try {
            return CID.create(node.addFile(target.getAbsolutePath()));
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
        return null;
    }

    @NonNull
    public Reader getReader(@NonNull CID cid) throws Exception {
        return node.getReader(cid.getCid());
    }


    private boolean loadToOutputStream(@NonNull OutputStream outputStream, @NonNull CID cid,
                                       @NonNull Progress progress) {

        try (InputStream inputStream = getLoaderStream(cid, progress)) {
            IPFS.copy(inputStream, outputStream);
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
            return false;
        }
        return true;

    }

    private void getToOutputStream(@NonNull OutputStream outputStream, @NonNull CID cid) throws Exception {
        try (InputStream inputStream = getInputStream(cid, 4096)) {
            IPFS.copy(inputStream, outputStream);
        }
    }

    public File getFile(@NonNull CID cid) throws Exception {
        File file = getCacheFile(cid);
        if (!file.exists()) {
            file = createCacheFile(cid);
            try (FileOutputStream outputStream = new FileOutputStream(file)) {
                try (InputStream inputStream = getInputStream(cid, 4096)) {
                    IPFS.copy(inputStream, outputStream);
                }
            }
        }
        return file;
    }

    public boolean loadToFile(@NonNull File file, @NonNull CID cid, @NonNull Progress progress) {
        checkDaemon();

        try (FileOutputStream outputStream = new FileOutputStream(file)) {
            return loadToOutputStream(outputStream, cid, progress);
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
            return false;
        }
    }

    public void storeToOutputStream(@NonNull OutputStream os, @NonNull ReaderProgress progress,
                                    @NonNull CID cid, int blockSize) throws Exception {

        long totalRead = 0L;
        int remember = 0;
        Reader reader = getReader(cid);
        try {

            reader.load(blockSize);
            long read = reader.getRead();
            while (read > 0) {

                if (progress.isClosed()) {
                    throw new RuntimeException("Progress closed");
                }

                // calculate progress
                totalRead += read;
                if (progress.doProgress()) {
                    if (progress.getSize() > 0) {
                        int percent = (int) ((totalRead * 100.0f) / progress.getSize());
                        if (remember < percent) {
                            remember = percent;
                            progress.setProgress(percent);
                        }
                    }
                }

                byte[] bytes = reader.getData();
                os.write(bytes, 0, bytes.length);

                reader.load(blockSize);
                read = reader.getRead();
            }
        } finally {
            reader.close();
        }

    }

    public void storeToOutputStream(@NonNull OutputStream os, @NonNull CID cid, int blockSize) throws Exception {

        Reader reader = getReader(cid);
        try {

            reader.load(blockSize);
            long read = reader.getRead();
            while (read > 0) {
                byte[] bytes = reader.getData();
                os.write(bytes, 0, bytes.length);

                reader.load(blockSize);
                read = reader.getRead();
            }
        } finally {
            reader.close();
        }

    }


    @NonNull
    private Loader getLoader(@NonNull CID cid, @NonNull Closeable closeable) throws Exception {
        return node.getLoader(cid.getCid(), closeable::isClosed);

    }

    @NonNull
    public InputStream getLoaderStream(@NonNull CID cid, @NonNull Closeable closeable, long readTimeoutMillis) throws Exception {

        Loader loader = getLoader(cid, closeable);

        return new CloseableInputStream(loader, readTimeoutMillis);

    }

    @NonNull
    private InputStream getLoaderStream(@NonNull CID cid, @NonNull Progress progress) throws Exception {

        Loader loader = getLoader(cid, progress);

        return new LoaderInputStream(loader, progress);

    }

    public void storeToFile(@NonNull File file, @NonNull CID cid, int blockSize) throws Exception {

        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            storeToOutputStream(fileOutputStream, cid, blockSize);
        }

    }

    @NonNull
    File getCacheDir() {
        return this.cacheDir;
    }

    @NonNull
    public File createCacheFile() throws IOException {
        return File.createTempFile("temp", ".cid", getCacheDir());
    }

    public File getCacheFile(@NonNull CID cid) {
        return new File(getCacheDir(), cid.getCid());
    }

    @NonNull
    public File createCacheFile(@NonNull CID cid) throws IOException {

        File file = getCacheFile(cid);
        if (file.exists()) {
            boolean result = file.delete();
            if (!result) {
                LogUtils.info(TAG, "Deleting failed");
            }
        }
        boolean succes = file.createNewFile();
        if (!succes) {
            LogUtils.info(TAG, "Failed create a new file");
        }
        return file;
    }

    @Nullable
    public CID storeInputStream(@NonNull InputStream inputStream, @NonNull ReaderProgress progress) {


        String res = "";
        try {
            res = node.stream(new WriterStream(inputStream, progress));
        } catch (Throwable e) {
            if (!progress.isClosed()) {
                LogUtils.error(TAG, e);
            }
        }

        if (!res.isEmpty()) {
            return CID.create(res);
        }
        return null;
    }

    @Nullable
    public CID storeInputStream(@NonNull InputStream inputStream) {

        return storeInputStream(inputStream, new ReaderProgress() {
            @Override
            public boolean isClosed() {
                return false;
            }

            @Override
            public boolean isOffline() {
                return false;
            }

            @Override
            public void setProgress(int progress) {

            }

            @Override
            public boolean doProgress() {
                return false;
            }

            @Override
            public long getSize() {
                return 0;
            }

            @Override
            public void setTotalRead(long totalRead) {

            }
        });
    }

    @Nullable
    public String getText(@NonNull CID cid) {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            getToOutputStream(outputStream, cid);
            return new String(outputStream.toByteArray());
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
            return null;
        }
    }

    @Nullable
    public byte[] getData(@NonNull CID cid) {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            getToOutputStream(outputStream, cid);
            return outputStream.toByteArray();
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
            return null;
        }
    }

    @Nullable
    public byte[] loadData(@NonNull CID cid, @NonNull Progress progress) {
        checkDaemon();
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            boolean success = loadToOutputStream(outputStream, cid, progress);
            if (success) {
                return outputStream.toByteArray();
            } else {
                return null;
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
            return null;
        }

    }

    public void gc() {
        try {
            node.repoGC();
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }

    }

    @Override
    public void error(String message) {
        if (message != null && !message.isEmpty()) {
            LogUtils.error(TAG, message);
        }
    }

    @Override
    public void info(String message) {
        if (message != null && !message.isEmpty()) {
            LogUtils.info(TAG, "" + message);
        }
    }

    @Override
    public void reachablePrivate() {
        setReachable(Reachable.PRIVATE);
    }

    @Override
    public void reachablePublic() {
        setReachable(Reachable.PUBLIC);
    }

    @Override
    public void reachableUnknown() {
        setReachable(Reachable.UNKNOWN);
    }

    @Override
    public void verbose(String s) {
        LogUtils.verbose(TAG, "" + s);
    }


    @NonNull
    public InputStream getInputStream(@NonNull CID cid, int blockSize) throws Exception {
        Reader reader = getReader(cid);
        return new ReaderInputStream(reader, blockSize);

    }

    public boolean isDaemonRunning() {
        return node.getRunning();
    }

    public boolean isValidCID(String multihash) {
        try {
            this.node.cidCheck(multihash);
            return true;
        } catch (Throwable e) {
            return false;
        }
    }

    public boolean isValidPID(String multihash) {
        try {
            this.node.pidCheck(multihash);
            return true;
        } catch (Throwable e) {
            return false;
        }
    }

    public long getSeeded() {
        return seeded;
    }

    @Override
    public void seeding(long amount) {
        seeded += amount;

        LogUtils.info(TAG, "Seeding Amount : " + amount);
    }

    public boolean isEmptyDir(@NonNull CID cid) {
        return Objects.equals(cid.getCid(), EMPTY_DIR_32) || Objects.equals(cid.getCid(), EMPTY_DIR_58);
    }

    public boolean isDir(@NonNull CID doc, @NonNull Closeable closeable) {
        List<LinkInfo> links = getLinks(doc, closeable);
        return links != null && !links.isEmpty();
    }

    public long getSize(@NonNull CID cid, @NonNull Closeable closeable) {
        List<LinkInfo> links = ls(cid, closeable);
        int size = -1;
        if (links != null) {
            for (LinkInfo info : links) {
                size += info.getSize();
            }
        }
        return size;
    }

    public long numSwarmPeers() {
        checkDaemon();
        return node.numSwarmPeers();
    }


    private static class LoaderInputStream extends InputStream implements AutoCloseable {
        private final Loader mLoader;
        private final Progress mProgress;
        private final long size;
        private int position = 0;
        private byte[] data = null;
        private int remember = 0;
        private long totalRead = 0L;

        LoaderInputStream(@NonNull Loader loader, @NonNull Progress progress) {
            mLoader = loader;
            mProgress = progress;
            size = mLoader.getSize();
        }

        @Override
        public int available() {
            long size = mLoader.getSize();
            return (int) size;
        }

        @Override
        public int read() throws IOException {


            try {
                if (data == null) {
                    invalidate();
                    preLoad();
                }
                if (data == null) {
                    return -1;
                }
                if (position < data.length) {
                    byte value = data[position];
                    position++;
                    return (value & 0xff);
                } else {
                    invalidate();
                    if (preLoad()) {
                        byte value = data[position];
                        position++;
                        return (value & 0xff);
                    } else {
                        return -1;
                    }
                }

            } catch (Throwable e) {
                throw new IOException(e);
            }
        }


        private void invalidate() {
            position = 0;
            data = null;
        }


        private boolean preLoad() throws Exception {

            mLoader.load(4096, mProgress::isClosed);
            int read = (int) mLoader.getRead();
            if (read > 0) {
                data = new byte[read];
                byte[] values = mLoader.getData();
                System.arraycopy(values, 0, data, 0, read);

                totalRead += read;
                if (mProgress.doProgress()) {
                    if (size > 0) {
                        int percent = (int) ((totalRead * 100.0f) / size);
                        if (remember < percent) {
                            remember = percent;
                            mProgress.setProgress(percent);
                        }
                    }
                }
                return true;
            }
            return false;
        }

        public void close() {
            try {
                mLoader.close();
            } catch (Throwable e) {
                LogUtils.error(TAG, e);
            }
        }
    }

    private static class ReaderInputStream extends InputStream implements AutoCloseable {
        private final Reader mReader;
        private final int mBlockSize;
        private int position = 0;
        private byte[] data = null;

        ReaderInputStream(@NonNull Reader reader, int blockSize) {
            mReader = reader;
            mBlockSize = blockSize;
        }

        @Override
        public int available() {
            long size = mReader.getSize();
            return (int) size;
        }


        @Override
        public int read() throws IOException {

            try {
                if (data == null) {
                    invalidate();
                    preLoad();
                }
                if (data == null) {
                    return -1;
                }
                if (position < data.length) {
                    byte value = data[position];
                    position++;
                    return (value & 0xff);
                } else {
                    invalidate();
                    if (preLoad()) {
                        byte value = data[position];
                        position++;
                        return (value & 0xff);
                    } else {
                        return -1;
                    }
                }


            } catch (Throwable e) {
                throw new IOException(e);
            }
        }

        private void invalidate() {
            position = 0;
            data = null;
        }


        private boolean preLoad() throws Exception {
            mReader.load(mBlockSize);
            int read = (int) mReader.getRead();
            if (read > 0) {
                data = new byte[read];
                byte[] values = mReader.getData();
                System.arraycopy(values, 0, data, 0, read);
                return true;
            }
            return false;
        }

        public void close() {
            try {
                mReader.close();
            } catch (Throwable e) {
                LogUtils.error(TAG, e);
            }
        }
    }



    private static class CloseableInputStream extends InputStream implements AutoCloseable {
        private final Loader mLoader;
        private final long readTimeoutMillis;
        private int position = 0;
        private byte[] data = null;

        CloseableInputStream(@NonNull Loader loader, long readTimeoutMillis) {
            this.mLoader = loader;
            this.readTimeoutMillis = readTimeoutMillis;
        }

        @Override
        public int available() {
            long size = mLoader.getSize();
            return (int) size;
        }

        @Override
        public int read() throws IOException {


            try {
                if (data == null) {
                    invalidate();
                    preLoad();
                }
                if (data == null) {
                    return -1;
                }
                if (position < data.length) {
                    byte value = data[position];
                    position++;
                    return (value & 0xff);
                } else {
                    invalidate();
                    if (preLoad()) {
                        byte value = data[position];
                        position++;
                        return (value & 0xff);
                    } else {
                        return -1;
                    }
                }

            } catch (Throwable e) {
                throw new IOException(e);
            }
        }


        private void invalidate() {
            position = 0;
            data = null;
        }


        private boolean preLoad() throws Exception {
            long start = System.currentTimeMillis();
            mLoader.load(4096, () -> (System.currentTimeMillis() - start) > (readTimeoutMillis));
            int read = (int) mLoader.getRead();
            if (read > 0) {
                data = new byte[read];
                byte[] values = mLoader.getData();
                System.arraycopy(values, 0, data, 0, read);
                return true;
            }
            return false;
        }

        public void close() {
            try {
                mLoader.close();
            } catch (Throwable e) {
                LogUtils.error(TAG, e);
            }
        }
    }

    private static class WriterStream implements lite.WriterStream {
        private final InputStream mInputStream;
        private final ReaderProgress mProgress;
        private final int SIZE = 262144;
        private int progress = 0;
        private long totalRead = 0;
        private byte[] data = new byte[SIZE];


        WriterStream(@NonNull InputStream inputStream, @NonNull ReaderProgress progress) {
            this.mInputStream = inputStream;
            this.mProgress = progress;
        }


        @Override
        public byte[] data() {
            return data;
        }

        @Override
        public long read() throws Exception {


            if (mProgress.isClosed()) {
                throw new Exception("progress closed");
            }


            int read = mInputStream.read(data);

            totalRead += read;

            if (mProgress.doProgress()) {
                long streamSize = mProgress.getSize();
                if (streamSize > 0) {
                    int percent = (int) ((totalRead * 100.0f) / streamSize);
                    if (progress < percent) {
                        progress = percent;
                        mProgress.setProgress(percent);
                    }
                }
            }
            return read;
        }

        @Override
        public boolean close() {
            return mProgress.isClosed();

        }
    }
}
