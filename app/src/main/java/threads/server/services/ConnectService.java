package threads.server.services;

import android.content.Context;

import androidx.annotation.NonNull;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import threads.LogUtils;
import threads.server.InitApplication;
import threads.server.core.peers.PEERS;
import threads.server.core.peers.User;
import threads.server.ipfs.IPFS;
import threads.server.ipfs.PID;

public class ConnectService {
    private static final String TAG = ConnectService.class.getSimpleName();

    public static void connect(@NonNull Context context) {
        List<User> users = PEERS.getInstance(context).getUsers();
        if (!users.isEmpty()) {
            ExecutorService executor = Executors.newFixedThreadPool(users.size());

            for (User user : users) {
                executor.submit(() -> connect(context, PID.create(user.getPid())));
            }
            int timeout = InitApplication.getConnectionTimeout(context);
            shutdownAndAwaitTermination(executor, timeout);
        }
    }

    private static void shutdownAndAwaitTermination(@NonNull ExecutorService pool, int timeout) {
        pool.shutdown(); // Disable new tasks from being submitted
        try {
            // Wait a while for existing tasks to terminate
            if (!pool.awaitTermination(timeout, TimeUnit.SECONDS)) {
                pool.shutdownNow(); // Cancel currently executing tasks
                // Wait a while for tasks to respond to being cancelled
                if (!pool.awaitTermination(timeout, TimeUnit.SECONDS)) {
                    LogUtils.info(TAG, "Pool did not terminate");
                }
            }
        } catch (InterruptedException ie) {
            // (Re-)Cancel if current thread also interrupted
            pool.shutdownNow();
            // Preserve interrupt status
            Thread.currentThread().interrupt();
        }
    }

    private static boolean connect(@NonNull Context context, @NonNull PID pid) {

        IPFS ipfs = IPFS.getInstance(context);

        if (ipfs.isConnected(pid)) {
            return true;
        }

        // now check old addresses
        PEERS peers = PEERS.getInstance(context);
        User user = peers.getUserByPID(pid);
        Objects.requireNonNull(user);
        String address = user.getAddress();
        if (!address.isEmpty() && !address.contains("p2p-circuit")) {
            String multiAddress = address.concat("/p2p/" + pid.getPid());

            if (ipfs.swarmConnect(multiAddress, 5)) {
                return true;
            }
        }


        return ipfs.swarmConnect(pid, 15);


    }
}
