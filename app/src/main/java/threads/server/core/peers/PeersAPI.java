package threads.server.core.peers;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import threads.server.ipfs.PID;


public class PeersAPI {


    private final UsersDatabase usersDatabase;

    PeersAPI(@NonNull UsersDatabase usersDatabase) {

        this.usersDatabase = usersDatabase;
    }

    @NonNull
    public UsersDatabase getUsersDatabase() {
        return usersDatabase;
    }


    public void storeUser(@NonNull User user) {

        getUsersDatabase().userDao().insertUsers(user);
    }


    @NonNull
    public User createUser(@NonNull PID pid, @NonNull String name) {

        return User.createUser(name, pid);
    }


    public void removeUsers(@NonNull String... pids) {
        getUsersDatabase().userDao().removeUserByPids(pids);
    }


    public boolean existsUser(@NonNull PID user) {

        return existsUser(user.getPid());
    }

    public boolean existsUser(@NonNull String pid) {

        return getUsersDatabase().userDao().hasUser(pid) > 0;
    }


    @Nullable
    public User getUserByPID(@NonNull PID pid) {
        return getUserByPID(pid.getPid());
    }


    @Nullable
    public User getUserByPID(@NonNull String pid) {
        return getUsersDatabase().userDao().getUserByPid(pid);
    }

    @NonNull
    public List<User> getUsers() {
        return getUsersDatabase().userDao().getUsers();
    }


    public void setUserConnected(@NonNull PID user) {

        getUsersDatabase().userDao().setConnected(user.getPid());

        getUsersDatabase().userDao().setTimestamp(user.getPid(), System.currentTimeMillis());
    }

    public void setUserDisconnected(@NonNull PID user) {

        getUsersDatabase().userDao().setDisconnected(user.getPid());
    }


    public void setUserWork(@NonNull String pid, @NonNull UUID id) {
        getUsersDatabase().userDao().setWork(pid, id.toString());
    }

    public void resetUserWork(@NonNull String pid) {
        getUsersDatabase().userDao().resetWork(pid);
    }

    public void setUserDialing(@NonNull String pid) {

        getUsersDatabase().userDao().setDialing(pid, true);
    }

    public void resetUserDialing(@NonNull String pid) {

        getUsersDatabase().userDao().setDialing(pid, false);
    }


    public boolean isUserConnected(@NonNull PID user) {

        return isUserConnected(user.getPid());
    }

    private boolean isUserConnected(@NonNull String pid) {

        return getUsersDatabase().userDao().isConnected(pid);
    }


    @Nullable
    public String getUserPublicKey(@NonNull String pid) {

        return getUsersDatabase().userDao().getPublicKey(pid);
    }


    public void setUserAlias(@NonNull String pid, @NonNull String alias) {

        getUsersDatabase().userDao().setAlias(pid, alias);
    }


    @NonNull
    public List<PID> getUsersPIDs() {
        List<PID> result = new ArrayList<>();
        List<String> pids = getUsersDatabase().userDao().getUserPids();
        for (String pid : pids) {
            result.add(PID.create(pid));
        }
        return result;
    }


    public boolean getUserIsLite(@NonNull String pid) {

        return getUsersDatabase().userDao().isLite(pid);
    }

    public void setUserPublicKey(@NonNull String pid, @NonNull String pkey) {
        getUsersDatabase().userDao().setPublicKey(pid, pkey);
    }

    public void setUserAgent(@NonNull String pid, @NonNull String agent) {
        getUsersDatabase().userDao().setAgent(pid, agent);
    }

    public void setUserAddress(@NonNull String pid, @NonNull String address) {
        getUsersDatabase().userDao().setAddress(pid, address);
    }

    public void setUserLite(@NonNull String pid) {
        getUsersDatabase().userDao().setLite(pid);
    }


    public void setUsersInvisible(String... pids) {
        getUsersDatabase().userDao().setInvisible(pids);
    }

    public void setUsersVisible(String... pids) {
        getUsersDatabase().userDao().setVisible(pids);
    }
}
